# Box

Box és un gestor de màquines virtuals.

La documentació està a <https://xtec.dev/tool/box/>

El mòdul es publica a [Powershell Gallery](https://www.powershellgallery.com/packages/Box).

## Develop

```pwsh
> Import-Module .\Box\Box.psd1 -Force
```

Testing:

```pwsh
> Import-Module Build.psm1 -Force
Test-Box
```

## Publish

Actualitza la versió al fitxer `Box.psd1`.

Necessites obtenir una clau `key.txt`.

Instal.la versió 7 powershell amb scoop.

Executa:

```pwsh
> pwsh.exe
> Import-Module Build.psm1 -Force
> Publish-Box
```



