# OneDrive Fix
$Documents = [System.Environment]::GetFolderPath('MyDocuments')
if (-Not($env:PSModulePath -Contains $Documents)) {
  $ModulesPath = "$([System.Environment]::GetFolderPath('MyDocuments'))\WindowsPowerShell\Modules"
  $env:PSModulePath = "$ModulesPath;$env:PSModulePath"
}

function Debug-Box {
  Import-Module ./box.psm1 -Force
}

function Publish-Box {

  # Utilitzem un powershell actualitzat (insta.la amb scoop, pendent test)
  #pwsh.exe
  Publish-Module -Path Box -NuGetApiKey (Get-Content key.txt)
}

function Test-Box {
  #Install-Module -Name Pester -RequiredVersion 5.5.0 -Scope CurrentUser -SkipPublisherCheck

  Invoke-Pester VBox.Tests.ps1
}

Export-ModuleMember -Function Publish-Box, Test-Box

