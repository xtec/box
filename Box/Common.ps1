
function Get-BoxPath {
  param(
    [string] $Path
  )

  $boxPath = Join-Path $global:HOME ".box"
  if (-not(Test-Path -Path $boxPath -PathType Container)) {
    New-Item -Path $boxPath -ItemType Directory | Out-Null
  }

  if ($Path) {
    $boxPath = Join-Path $boxPath $Path
    if (-not(Test-Path -Path $boxPath -PathType Container)) {
      New-Item -Path $boxPath -ItemType Directory | Out-Null
    }
  }

  return $boxPath

}