BeforeAll {
  . $PSCommandPath.Replace('.Tests.ps1', '.ps1')
}

Describe 'Get-SshKey' {
  It 'Get the Host Only Adapter' {
    $key = Get-SshKey -Type "rsa"
    $key | Should -Not -BeNullOrEmpty
  }
}