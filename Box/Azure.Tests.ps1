$Documents = [System.Environment]::GetFolderPath('MyDocuments')
if (-Not($env:PSModulePath -Contains $Documents)) {
  $ModulesPath = "$([System.Environment]::GetFolderPath('MyDocuments'))\WindowsPowerShell\Modules"
  $env:PSModulePath = "$ModulesPath;$env:PSModulePath"
}

$modules = 'Az.Accounts', 'Az.Compute', 'Az.Network', 'Az.Resources', 'Az.Ssh'

foreach ($module in $modules) {
  if (Get-Module -ListAvailable -Name $module) {
    Write-Host("Removing module $module ...")
    Get-InstalledModule $module | Uninstall-Module -AllVersions -Force -Verbose
  }
}