function Install-Code {

  Install-Scoop vscode -Bucket "extras"
  

  # add image shortcut on desktop

  Install-CodeExtension "ms-vscode-remote.remote-ssh"
  Install-CodeExtension "ms-vscode-remote.remote-wsl"
  #Install-CodeExtension "ms-vscode.remote-server"

  $code = Join-Path $env:USERPROFILE "scoop/apps/vscode/current/Code"

  ## Desktop icon
  $icon = "$([Environment]::GetFolderPath("Desktop"))\Code.lnk"
  if (-Not(Test-Path -Path $icon -PathType leaf -ErrorAction SilentlyContinue)) {
    $wshShell = New-Object -comObject WScript.Shell
    $shortcut = $wshShell.CreateShortcut($icon)
    $shortcut.TargetPath = $code
    $shortcut.Save()
    Write-Host "Added Code shortcut on Desktop"
  }

}
function Install-CodeExtension {
  param (
    [string] $Name
  )

  $code = Join-Path $env:USERPROFILE "scoop/apps/vscode/current/bin/code.cmd"

  $extensions = Invoke-Expression "$code --list-extensions"
  if (-Not($extensions -Contains $Name)) {
    Invoke-Expression "$code --install-extension $Name"
  }
}

function Install-Idea {

  # Groovy still not supports java22
  Install-Scoop openjdk21 -Bucket "java"
  Install-Scoop idea -Bucket "extras"

  $studio = Join-Path $env:USERPROFILE "scoop/apps/idea/current/IDE/bin/idea64.exe"

  ## Desktop icon
  $icon = "$([Environment]::GetFolderPath("Desktop"))\IDEA.lnk"
  if (-Not(Test-Path -Path $icon -PathType leaf -ErrorAction SilentlyContinue)) {
    $wshShell = New-Object -comObject WScript.Shell
    $shortcut = $wshShell.CreateShortcut($icon)
    $shortcut.TargetPath = $studio
    $shortcut.Save()
    Write-Host "Added IDEA shortcut on Desktop"
  }
}


function Install-Studio {

  Install-Scoop openjdk22 -Bucket "java"
  Install-Scoop android-studio -Bucket "extras"
  #Install-Scoop android-clt
  #Install-Scoop flutter


  # TODO
  $studio = Join-Path $env:USERPROFILE "scoop/apps/android-studio/current/bin/studio64.exe"

  ## Desktop icon
  $icon = "$([Environment]::GetFolderPath("Desktop"))\Studio.lnk"
  if (-Not(Test-Path -Path $icon -PathType leaf -ErrorAction SilentlyContinue)) {
    $wshShell = New-Object -comObject WScript.Shell
    $shortcut = $wshShell.CreateShortcut($icon)
    $shortcut.TargetPath = $studio
    $shortcut.Save()
    Write-Host "Added Studio shortcut on Desktop"
  }
}
