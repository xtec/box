# OneDrive Fix
$Documents = [System.Environment]::GetFolderPath('MyDocuments')
if (-Not($env:PSModulePath -Contains $Documents)) {
  $ModulesPath = "$([System.Environment]::GetFolderPath('MyDocuments'))\WindowsPowerShell\Modules"
  $env:PSModulePath = "$ModulesPath;$env:PSModulePath"
}

$scripts = Get-ChildItem -Path $PSScriptRoot\*.ps1 -Exclude *.Tests.ps1, Build.ps1 -ErrorAction SilentlyContinue

Foreach ($script in @($scripts)) {
  Try {
    . $script.fullname
  }
  Catch {
    Write-Error -Message "Failed to import function $($script.fullname): $_"
  }
}

Export-ModuleMember -Function 'Connect-Azure', 'New-Azure', 'Remove-Azure', 'Update-Code', 'Start-Code', 'New-Hetzner', 'Remove-Hetzner', 'Connect-Isard', 'Get-Isard', 'New-Isard', 'Remove-Isard', 'Start-Isard', 'Stop-Isard', 'Start-Proxy', 'Connect-Ssh', 'Connect-Wsl', 'Get-Wsl', 'New-Wsl', 'Remove-Wsl', 'Start-Wsl', 'Stop-Wsl', 'Update-Wsl', 'Install-Code', 'Install-Idea', 'Install-Studio' 

