function Install-Scoop {
  param(
    [Parameter(Position = 0, Mandatory = $True)]
    [String] $App,
    [String] $Bucket
  )

  if (-Not(Get-Command "scoop" -ErrorAction SilentlyContinue)) { 
    Write-Host "Installing scoop"
    Invoke-RestMethod -Uri https://get.scoop.sh | Invoke-Expression
  }

  if (-Not(Get-Command "git" -ErrorAction SilentlyContinue)) {
    scoop install git
  }

  $path = Get-ScoopPath $App
  if (-Not(Test-Path -Path $path -PathType leaf -ErrorAction SilentlyContinue)) {

    if ($Bucket) {
      scoop bucket add $Bucket
      scoop install "$Bucket/$App"
    }
    else {
      scoop install $App
    }
  }
}

function Get-ScoopPath {
  param(
    [String] $App
  )

  $path = Join-Path $env:USERPROFILE "scoop/apps/"
  $path = Join-Path $path $App
  return $path
}