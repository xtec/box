function Install-WireProxy {

    if (-Not(Get-Command "wireproxy" -ErrorAction SilentlyContinue)) {
        Install-Scoop 
        # https://github.com/pufferffish/wireproxy
        scoop install main/wireproxy
    }    
}

# ssh  -D 127.0.0.1:2222 isard@10.2.76.37
# ssh -vvv -J 127.0.0.1:2222  isard@10.2.76.37
# ssh -o ProxyCommand="wireproxy -c $HOME/.box/proxy/wireproxy.conf" isard@10.2.76.37
function Start-WireProxy {
    param (
        #[Parameter(position = 0, mandatory = $True)]
        #[string] $Address
        [string] $Conf
    )

    Install-WireProxy

    $proxy_path = Get-BoxPath -Path "wireproxy"

    #TODO afegir /32 a isard-vpn.conf
    $wg_conf = Join-Path $proxy_path "wireguard.conf"
    if ($Conf) {
      Copy-Item -Path $Conf -Destination $wg_conf 
    }
    
    $proxy_conf = @"
WGConfig = ${wg_conf}

[TCPClientTunnel]
BindAddress = 127.0.0.1:7637
Target = ${Address}
"@ 

    $proxy_conf = @"
WGConfig = ${wg_conf}

[Socks5]
BindAddress = 127.0.0.1:2222
"@ 

    $proxy_conf_path = Join-Path $proxy_path "wireproxy.conf"
    [IO.File]::WriteAllLines($proxy_conf_path, $proxy_conf)
    

    wireproxy -c $proxy_conf_path   
}