 
# https://www.lucd.info/2019/12/06/cloud-init-part-1-the-basics/
# https://gist.github.com/leogallego/a614c61457ed22cb1d960b32de4a1b01
function Get-OVF {


} 

function Get-Ubuntu {
    param (
        [string] $Path,
        [string] $File,
        [bool] $Update
    )

    $cacheDir = Get-BoxPath -Path "cache"
    $imageFile = Join-Path $cacheDir $File

    # If not update and image file exists we have it

    if (-Not($Update)) {
        if (Test-Path $imageFile -PathType Leaf) {
            return $imageFile
        }
    }

    $ProgressPreference = "SilentlyContinue"

    $url = (("https://cloud-images.ubuntu.com", $Path, $File) -join "/")
    Write-Verbose $url

    # If update and we have a file, check if there's one newer

    if (Test-Path $imageFile -PathType Leaf) {
        Write-UbuntuLog
        Write-Host $File -NoNewline
        Write-Host -ForegroundColor Yellow " (imatge)"
        Write-UbuntuLog
        Write-Host "Comprovant si hi ha una imatge més recent ..." -NoNewline
        $response = Invoke-WebRequest $url -UseBasicParsing -Method Head
        Write-Host -ForegroundColor Green " Fet."

        $cloudFileTime = Get-Date -Date ($response.Headers["Last-Modified"])
        $cacheFileTime = (Get-ChildItem $imageFile | Select-Object LastWriteTime).LastWriteTime

        Write-Debug "Cache file time: $cacheFileTime"
        Write-Debug "Cloud file time: $cloudFileTime"
        if ($cacheFileTime -gt $cloudFileTime) {
            Write-UbuntuLog
            Write-Host "Tens en cache la imatge mes recent " -NoNewline
            Write-Host -ForegroundColor Yellow "($cloudFileTime)"
            return $imageFile
        }
    }

    # Download file
    Write-UbuntuLog
    Write-Host "Imatge de $url"
    Write-UbuntuLog
    Write-Host "Comprovant el tamany de la nova imatge ..." -NoNewline
    # TODO we must handle better this error
    try {
        $response = Invoke-WebRequest $url -UseBasicParsing -Method Head
    }
    catch {
        Write-Host
        Write-UbuntuLog
        Write-Host $url -ForegroundColor Red
        Write-UbuntuLog
        Write-Host $_
        exit
    }
    Write-Host -ForegroundColor Green " Fet."
    $downloadSize = [int]$response.Headers["Content-Length"]


    $tmpFile = Join-Path $cacheDir "${File}.tmp"
    if (Test-Path $tmpFile -PathType Leaf) {
        Remove-Item $tmpFile
    }
    
    Write-UbuntuLog
    Write-Host "Descarregant la nova imatge ($([int]($downloadSize / 1024 / 1024)) MB)..." -NoNewline
    

    Invoke-WebRequest $url -OutFile $tmpFile

    Write-Host -ForegroundColor Green " Fet."

    # Check file hash
    $hashPath = (("https://cloud-images.ubuntu.com", $Path, "SHA256SUMS") -join "/")
    Write-UbuntuLog
    Write-Host "Verificant el hash del fitxer ..." -NoNewline
    $hashSums = [System.Text.Encoding]::UTF8.GetString((Invoke-WebRequest $hashPath -UseBasicParsing).Content)
    $fileHash = Get-FileHash $tmpFile -Algorithm SHA256
    if (($hashSums | Select-String -pattern $fileHash.Hash -SimpleMatch).Count -eq 0) { 
        Remove-Item $tmpFile
        Write-UbuntuLog
        Write-Host -ForegroundColor Red "La imatge no s'ha descarregat correctament." 
        return
    }
    Write-Host -ForegroundColor Green " Fet."

    $ProgressPreference = "Continue"

    # Replace file

    if (Test-Path $imageFile -PathType Leaf) {
        Remove-Item $imageFile
    }

    Move-Item $tmpFile $imageFile

    return $imageFile
}

function Update-Ubuntu {
    param (
        [string] $Path,
        [string] $File
    )

    Get-Ubuntu -Path $Path -File $File -Update $True
}

##### Private
function Write-UbuntuLog {
    param(
        [string] $File
    )
  
    Write-Host -ForegroundColor Blue -NoNewline "Ubuntu "
    Write-Host ": " -NoNewline
}