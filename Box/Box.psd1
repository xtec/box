@{

  RootModule        = 'Box.psm1'
  ModuleVersion     = '0.1.18'
  # Supported PSEditions
  # CompatiblePSEditions = @()
  GUID              = 'c07f289a-8305-4691-8101-c7b363f1448d'
  Author            = 'David de Mingo'
  CompanyName       = 'xtec.dev'
  Copyright         = '(c) 2024 David de Mingo. All rights reserved.'
  Description       = 'Gestor de màquines virtuals'

  # Minimum version of the Windows PowerShell engine required by this module
  # PowerShellVersion = ''

  # Name of the Windows PowerShell host required by this module
  # PowerShellHostName = ''

  # Minimum version of the Windows PowerShell host required by this module
  # PowerShellHostVersion = ''

  # Minimum version of Microsoft .NET Framework required by this module. This prerequisite is valid for the PowerShell Desktop edition only.
  # DotNetFrameworkVersion = ''

  # Minimum version of the common language runtime (CLR) required by this module. This prerequisite is valid for the PowerShell Desktop edition only.
  # CLRVersion = ''

  # Processor architecture (None, X86, Amd64) required by this module
  # ProcessorArchitecture = ''

  # Modules that must be imported into the global environment prior to importing this module
  RequiredModules   = @(
  
  )

  # Assemblies that must be loaded prior to importing this module
  # RequiredAssemblies = @()

  # Script files (.ps1) that are run in the caller's environment prior to importing this module.
  # ScriptsToProcess = @()

  # Type files (.ps1xml) to be loaded when importing this module
  # TypesToProcess = @()

  # Format files (.ps1xml) to be loaded when importing this module
  # FormatsToProcess = @()

  # Modules to import as nested modules of the module specified in RootModule/ModuleToProcess
  # NestedModules = @()

  FunctionsToExport = 'Connect-Azure', 'New-Azure', 'Remove-Azure', 'New-Hetzner', 'Remove-Hetzner', 'Connect-Isard', 'Get-Isard', 'New-Isard', 'Remove-Isard', 'Start-Isard', 'Stop-Isard', 'Start-Proxy', 'Connect-Ssh', 'Install-Studio', 'Connect-VBox', 'Connect-Wsl', 'Get-Wsl', 'New-Wsl', 'Remove-Wsl', 'Start-Wsl', 'Stop-Wsl', 'Update-Wsl', 'Install-Code', 'Install-Idea', 'Install-Studio' 

  # Cmdlets to export from this module, for best performance, do not use wildcards and do not delete the entry, use an empty array if there are no cmdlets to export.
  CmdletsToExport   = '*'

  # Variables to export from this module
  VariablesToExport = '*'

  # Aliases to export from this module, for best performance, do not use wildcards and do not delete the entry, use an empty array if there are no aliases to export.
  AliasesToExport   = '*'

  # DSC resources to export from this module
  # DscResourcesToExport = @()

  # List of all modules packaged with this module
  # ModuleList = @()

  # List of all files packaged with this module
  # FileList = @()

  # Private data to pass to the module specified in RootModule/ModuleToProcess. This may also contain a PSData hashtable with additional module metadata used by PowerShell.
  PrivateData       = @{
    PSData = @{

      # Tags applied to this module. These help with module discovery in online galleries.
      # Tags = @()
      LicenseUri = 'https://gitlab.com/xtec/box/-/raw/main/LICENSE'
      ProjectUri = 'https://gitlab.com/xtec/box/'
      IconUri    = 'https://xtec.dev/xtec.jpg'
      # ReleaseNotes = ''

    }
  } 

  # HelpInfo URI of this module
  # HelpInfoURI = ''

  # Default prefix for commands exported from this module. Override the default prefix using Import-Module -Prefix.
  # DefaultCommandPrefix = ''

}

