# Powershell

Get the source code of the PowerShell cmdlets:

(Get-Command $commandName).DLL;
(Get-Command $commandName).ImplementingType;

https://github.com/PowerShell/PowerShell