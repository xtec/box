# Powershelll

To view function contents:    (Get-Command <Function Name>).ScriptBlock


##

https://www.thomasmaurer.ch/2019/01/azure-cloud-shell/
https://techcommunity.microsoft.com/t5/itops-talk-blog/powershell-basics-connecting-to-vms-with-azure-psremoting/ba-p/428403

New-PSSession -HostName '4.180.82.94' -UserName box 

https://www.pluralsight.com/cloud-guru/labs/azure/connecting-to-azure-windows-vm-using-powershell

https://rohancragg.co.uk/misc/ps-module-paths/

With this command we can check PowerShell path:

```pwsh
> [System.Environment]::GetFolderPath('MyDocument')
> $env:PSModulePath.Split(';')
```

https://openwebinars.net/blog/crear-recursos-en-azure-con-powershell/
https://learn.microsoft.com/en-us/azure/virtual-machines/windows/connect-ssh?tabs=azurecli


#

https://johneverettcase.com/how-to-check-a-windows-service-status-with-powershell