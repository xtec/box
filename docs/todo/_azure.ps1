param(
  [String]
  $cmd = "help")

$global:ResourceGroupName = "box"
$global:Location = "westeurope"

$global:VirtualNetworkName = $global:ResourceGroupName
$global:SecurityGroupName = $global:ResourceGroupName

$global:sshKey = "$($env:USERPROFILE)\.ssh\id_ed25519"


# TODO no funciona
$OutputEncoding = [ System.Text.Encoding]::UTF8  


# https://learn.microsoft.com/en-us/azure/virtual-machines/windows/quick-create-powershell
class VM {

  static [void] create([String] $name, [String] $image) {
    Write-Host("Creant la màquina virtual $name")

    $password = Read-Host -AsSecureString -Prompt "Nova contrasenya per l'usuari 'box' a la màquina $name (ha de ser mínim 8 caràcters, 1 majúscula i un caràcter especial, per exemple 'P@ssword')"
    $credential = New-Object System.Management.Automation.PSCredential ('box', $password);
   
    New-AzVm -Name $name -ResourceGroupName $global:ResourceGroupName -Location $global:Location -Image $image -Credential $credential -VirtualNetworkName $global:VirtualNetworkName -SubnetName "$($global:VirtualNetworkName)-1" -SecurityGroupName $global:SecurityGroupName -OpenPorts 22, 3389, 5985 -PublicIpAddressName $name

    # OpenSSH

    Write-Host("Afegint un servidor SSH a la màquina virtual $name")

    # Get-WindowsCapability -Online | Where-Object Name -like 'OpenSSH.Server*'

    Invoke-AzVMRunCommand -ResourceGroupName $global:ResourceGroupName -VMName $name -CommandId 'RunPowerShellScript' -ScriptString "Add-WindowsCapability -Online -Name OpenSSH.Server~~~~0.0.1.0; Start-Service sshd; Set-Service -Name sshd -StartupType 'Automatic'"
    
    # Public Key

    $publicKey = Get-Content "$($env:USERPROFILE)\.ssh\id_ed25519.pub"
    
    Invoke-AzVMRunCommand -ResourceGroupName $global:ResourceGroupName -VMName $name -CommandId 'RunPowerShellScript' -ScriptString "'$($publicKey)' | Add-Content 'C:\ProgramData\ssh\administrators_authorized_keys';icacls.exe 'C:\ProgramData\ssh\administrators_authorized_keys' /inheritance:r /grant 'Administrators:F' /grant 'SYSTEM:F'"
  }

  static [void] ssh([String] $name) {

    # TODO when ssh create a public IP

    $ipAddress = (Get-AzPublicIpAddress -Name $name -ResourceGroupName $global:ResourceGroupName).IpAddress

    $ssh = "-i $($env:USERPROFILE)\.ssh\id_ed25519 -o UserKnownHostsFile=\\.\NUL -o StrictHostKeyChecking=no box@$($ipAddress)"
    Start-Process ssh $ssh

  }

  # Delete? Not used
  static [void] openPort([String] $name) {
    # Network Security Group (allow-SSH)

    $ruleName = 'box22'

    $securityGroup = Get-AzNetworkSecurityGroup -Name $global:SecurityGroupName -ResourceGroupName $global:ResourceGroupName

    $existsRule = $False
    foreach ($rule in $securityGroup.SecurityRules) {
      if ($rule.Name -eq $ruleName) {
        $existsRule = $True
      }
    }

    if (-not $existsRule) {
      Write-Host("Obrint el port 22 al grup de seguretat")
      $securityGroup | Add-AzNetworkSecurityRuleConfig -Name $ruleName -access Allow -Direction Inbound -Priority 1002 -SourceAddressPrefix '*' -SourcePortRange '*' -DestinationAddressPrefix '*' -DestinationPortRange 22 -Protocol TCP | Set-AzNetworkSecurityGroup
    }
  }

}
class SSH {
  static [void] acl([string] $file) {

    
    # https://learn.microsoft.com/en-us/windows-server/administration/openssh/openssh_keymanagement
    # https://benheater.com/setting-ssh-private-key-acls-in-powershell/
    # https://learn.microsoft.com/en-us/windows-server/administration/openssh/openssh_keymanagement
    # https://superuser.com/questions/1296024/windows-ssh-permissions-for-private-key-are-too-open

    icacls.exe $file /c /t /Inheritance:d # Remove Inheritance
    icacls.exe $file /c /t /Grant ${env:UserName}:F  # Set Ownership to Owner
    #Icacls $file /c /t /Remove Administrator "Authenticated Users" BUILTIN\Administrators BUILTIN\Administradores BUILTIN Everyone Todos System $file # Remove All Users, except for Owner
    icacls.exe $file /c /t /Remove Todos BUILTIN\Administradores
    icacls.exe $file /c /t /Remove Everyone BUILTIN\Administrators 
    #Icacls $file  # Verify
  }
}

##### MAIN #####

# We need to ensure that current user modules path is in PSModulePath because maybe Documents is renamed as Documentos
$documents = [System.Environment]::GetFolderPath('MyDocuments')
if (-Not($env:PSModulePath -Contains $documents)) {
  $ModulesPath = "$([System.Environment]::GetFolderPath('MyDocuments'))\WindowsPowerShell\Modules"
  $env:PSModulePath = "$ModulesPath;$env:PSModulePath"
}

# Install NuGet -MinimumVersion 2.8.5.201
if ((Get-PackageProvider -Name NuGet).version -lt 2.8.5.201 ) {
  try {
    Write-Host("Instal.lant el proveidor de paquets NuGet ...")
    Install-PackageProvider -Name NuGet -MinimumVersion 2.8.5.201 -Confirm:$False -Force 
  }
  catch [Exception] {
    $_.message 
    exit
  }
}

# Install Azure modules

# https://www.powershellgallery.com/packages?q=Az.

# TODO OneDrive sincronitza la carpeta Documents i Powershell guarda els moduls en la carpeta Documents. A l'escola no hi ha problemes, però en els ordinadors personals dels professors i els alumnes si 

$modules = 'Az.Accounts', 'Az.Compute', 'Az.Network', 'Az.Resources', 'Az.Ssh'

foreach ($module in $modules) {
  if (-Not(Get-Module -ListAvailable -Name $module)) {
    Write-Host("Instal.lant el mòdul $module ...")
    try {
      Install-Module -Name $module -Scope CurrentUser -Repository PSGallery -Confirm:$False -Force  
    }
    catch [Exception] {
      $_.message 
      exit
    }
  }
}

# TODO move to Class SSH
# Creem una clau SSH si no hi ha cap

if (-not(Test-Path -Path $HOME/.ssh -PathType Container)) {
  New-Item -Path $HOME/.ssh -ItemType Directory | Out-Null
}
if (-not(Test-Path -Path $HOME\.ssh\id_ed25519.pub -PathType Leaf)) {
  Write-Host("Creant una clau SSH per autenticarte a la màquina virtual d'azure")
  # No podem crear una clau sense contrasenya sense preguntar a l'usuari fent servir la opció -N '' perquè a vegades ha de ser '""', però a vegades ha de ser 'xxxxx' mínim 5 caracters i no admet '""'. 
  ssh-keygen -t ed25519 -f $HOME\.ssh\id_ed25519
  #ssh-keygen -o -a 100 -t ed25519 -C 'box' -f $HOME\.ssh\id_ed25519
  [SSH]::acl($global:sshKey)
  [SSH]::acl("$($global:sshKey).pub")  
}

# Conectem amb el compte d'Azure

if (!(Get-AzAccessToken -ErrorAction SilentlyContinue)) {
  Write-Host("Conectant el compte d'Azure")
  Connect-AzAccount
}


##### ARGS ####

switch ($cmd) {

  create {
    # Hem de crear un grup de recursos si aquest no existeix per les màquines, xarxes, etc.

    if (-not(Get-AzResourceGroup -Name $ResourceGroupName -ErrorAction SilentlyContinue)) {
      Write-Host("Creant un grup de recursos a ${ResourceGroupName}")
      New-AzResourceGroup -Name $ResourceGroupName -Location $Location
      
    }

    $name = $args[0]
    if ($Null -eq $name) {
      Write-Host('Has de dir quin és el nom de la màquina: azure.ps1 create <nom>')
      exit
    }
    $image = 'MicrosoftWindowsServer:WindowsServer:2022-datacenter-azure-edition:latest'

    [VM]::create($name, $image)
  }

  delete {
    $name = $args[0]
    if ($Null -eq $name) {
      Write-Host('Has de dir quin és el nom de la màquina: azure.ps1 delete <nom>')
      exit
    }
    if ($name -eq '*') {
      Remove-AzResourceGroup -Name $global:ResourceGroupName
    }
    else {
      Write-Host("Borrant la màquina virtual $($name)")
      Remove-AzVM -ResourceGroupName $global:ResourceGroupName -Name $name -Force -ForceDeletion $True
    }
  }

  list {

    # TODO check resource group exists

    Get-AzVM -Status

    Get-AzPublicIpAddress -ResourceGroupName $global:ResourceGroupName | Format-List -Property Name, IpAddress

    # Private address
    # (Get-AzNetworkInterface -Name $name -ResourceGroupName $global:ResourceGroupName).IPConfigurations
  }

  ssh {
    $name = $args[0]
    if ($Null -eq $name) {
      Write-Host('Has de dir quin és el nom de la màquina: azure.ps1 ssh <nom>')
      exit
    }

    [VM]::ssh($name)
  }

  dev {

  
  }

  Default {
    Write-Host("box.ps1 [command] 
    Commands:
    create <name>
    list
    ssh <name>
    delete <name>  
    ")
  }
}




