# WSL

* [WSL - Config](https://learn.microsoft.com/en-us/windows/wsl/wsl-config)
* [Cloud Init - Modul Reference](https://cloudinit.readthedocs.io/en/latest/reference/modules.html)

* <https://fizzylogic.nl/2023/01/05/how-to-configure-memory-limits-in-wsl2>

## TODO

* Escriure el fitxer /etc/hostname perquè el primer cop no actutalitza amb la configració de wsl.conf

* Hem d'afegir una opció per aïllar WSL del sistema de fitxers de Windows mitjnaçant la configuració de wsl.conf.

* Hem de crear una xarxa privada amb Wireguard per conectar les màquines WSL

## Code

Problemes amm root:

https://github.com/microsoft/vscode/issues/197955

## Cloud Init


[Tutorial - WSL](https://cloudinit.readthedocs.io/en/latest/tutorial/wsl.html)

Save the file to %USERPROFILE%\.cloud-init\${name}.user-data.

Verify that `cloud-init` ran successfully:

```sh
$ cloud-init status --wait
status: done
```

Now we can now see that `cloud-init` has detected that we running in WSL:

```sh
$ cloud-id
wsl
```

Verify our **user data**:

```sh
$ cloud-init query userdata
#cloud-config
...
```

We can also assert the user data we provided is a valid cloud-config:

```sh
cloud-init schema --system --annotate
```

### Mòduls

* [Write Files](https://cloudinit.readthedocs.io/en/latest/reference/modules.html#mod-cc-write-files)

### Datasources

El primer cop que arranca no es configura default user i hostname.

Si fem `wsl --shutdown <dist>` i tornem a arrencar `wsl -d <dist>` si.

https://cloudinit.readthedocs.io/en/latest/reference/datasources/wsl.html

## Config

https://learn.microsoft.com/en-us/windows/wsl/wsl-config

## Auto-Shutdown

https://github.com/microsoft/WSL/issues/9968

Microsoft designed WSL 2 so that the instance will automatically shutdown after you've closed the shells that keep it running.

```pwsh
wsl.exe --distribution Ubuntu-22.04 --exec dbus-launch true & wsl.exe --distribution Ubuntu-22.04
```

The dbus-launch command is used to start a session bus instance of dbus-daemon from a shell script.

[Sharing D-Bus among WSL2 Consoles](https://x410.dev/cookbook/wsl/sharing-dbus-among-wsl2-consoles/)

Many modern Unix/Linux GUI apps make use D-Bus for talking to one another and managing their lifecycle (ex. creating a single instance application or daemon).

## Default user

https://superuser.com/questions/1566022/how-to-set-default-user-for-manually-installed-wsl-distro/1566031#1566031

Verify the uid of the user that you want to set as default in your Linux console by id:

```sh
username@host:~$ id
uid=1000(username) gid=1000(username) groups=1000(username),4(adm),20(dialout),24(cdrom),25(floppy),27(sudo),29(audio),30(dip),44(video),46(plugdev),117(netdev),1001(docker)
username@host:~$ 
```

Edit the registry in Powershell:

Verify the distro name in Powershell by wsl -l -v:

Get-ItemProperty Registry::HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Lxss\*\ DistributionName

Get-ItemProperty Registry::HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Lxss\*\ DistributionName | Where-Object -Property DistributionName -eq esther  | Set-ItemProperty -Name DefaultUid -Value 1000

## LxRunOffline

https://github.com/DDoSolitary/LxRunOffline